package com.mvvm.hua.searchmovie.ui

import android.service.autofill.OnClickAction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.mvvm.hua.searchmovie.R
import com.mvvm.hua.searchmovie.models.Movie
import kotlinx.android.synthetic.main.layout_movie_list_item.view.*

class MovieRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<Movie> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_movie_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       when(holder){
           is MovieViewHolder ->{
               holder.bind(items.get(position))
           }
       }

    }

    fun submitList(movieList: ArrayList<Movie>){
        items = movieList
    }

    // To get the data to search Category
    fun filterList(filteredMovieList: ArrayList<Movie>) {
        this.items = filteredMovieList
        notifyDataSetChanged()
    }

    class MovieViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){
        val movie_id = itemView.movie_id
        val movie_title = itemView.movie_title
        val movie_popularity = itemView.movie_popularity
        val movie_video = itemView.movie_video

        fun bind(movie: Movie){
            movie_id.setText(movie.id.toString())
            movie_title.setText(movie.original_title)
            movie_popularity.setText(movie.popularity.toString())
            movie_video.setChecked(movie.video)

        }

    }
}
