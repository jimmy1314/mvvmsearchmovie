package com.mvvm.hua.searchmovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mvvm.hua.searchmovie.models.Movie
import com.mvvm.hua.searchmovie.ui.MovieRecyclerAdapter
import com.mvvm.hua.searchmovie.ui.TopSpacingItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class MainActivity : AppCompatActivity() {

    private lateinit var movieAdapter: MovieRecyclerAdapter
    var movies = arrayListOf<Movie>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ininitSeachView()
        ininitRecyclerView()
        addDataSet()

    }

    private fun readJson():ArrayList<Movie>
    {
        try {
            val inputSteam: InputStream = assets.open("small_movie_ids.json")
            var json = inputSteam.bufferedReader().use{it.readText()}


            var json_array = JSONArray(json)
            for ( i in 0..json_array.length()-1){
                var jsonObj = json_array.getJSONObject(i)
                var movie = Gson().fromJson<Movie>(jsonObj.toString(),Movie::class.java)
                movies.add(movie)
            }

            return movies
        }
        catch (e: IOException){
            return movies
        }
    }

    private fun sortByAlphabetical(): ArrayList<Movie>
    {
        var data = readJson()
        val c1: Comparator<Movie> = Comparator { o1, o2 ->
            o1.original_title.compareTo(o2.original_title)
        }
        data.sortWith(c1)
        return data

    }

    private fun addDataSet(){
        var sortedData = sortByAlphabetical()
        movieAdapter.submitList(sortedData)
    }

    private fun ininitRecyclerView()
    {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingDecorator = TopSpacingItemDecoration(30)
            addItemDecoration(topSpacingDecorator)
            movieAdapter = MovieRecyclerAdapter()
            adapter = movieAdapter
        }
    }

    private fun ininitSeachView(){
        movie_searcher.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    fun filter(keyword: String) {

        val filteredMovieSet: HashSet<Movie> = HashSet()

        val movieAry : ArrayList<Movie> = movies

        //The keyword with space is considered as OR
        var keywords = keyword.toLowerCase().split(" ")

        for (eachMovie in movieAry) {
            for (key in keywords) {
                if (eachMovie.original_title!!.toLowerCase().contains(key) || eachMovie.id!!.toString().contains(key))
                {
                    filteredMovieSet.add(eachMovie)
                }
            }
        }

        //calling a method of the adapter class and passing the filtered list
        movieAdapter.filterList(ArrayList(filteredMovieSet))
    }
}
