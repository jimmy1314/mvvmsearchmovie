package com.mvvm.hua.searchmovie.models

data class Movie(
    val id: Int,
    val original_title: String,
    val popularity: Double,
    val video: Boolean
)