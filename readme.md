# This application will allow the user to search by id or title in an edit text field. As the user type, the list will filter based on the search criteria.

## requirements:
- the application should load json data provided when the app is open.
- the main page shown when the app is open should show the list of movie data in the recyclerview
- each item row should show the - title of movie, id and rating
- provided search field on the top of the recyclerview or in the action bar
- when user types search keyword in the search field and clicks "enter/ search", the list in the recyclerview should be filtered to match the keyword with the movie title
(i.e. show only movie items that match the earch keyword)
- The search should match substring of movie title or id so search keyword "Avenge" should match the movie with title "Avengers"
- The keyword with space is considered as OR (ex. "this has 1123" will filter and find title with "this" or "has" or "1123")

## additional:
- Each row item shows video flag as checkbox
- The recyclerview list gets updated as user types the search keyword (i.e. without clicking "Enter/Search" button)
- Sort the list by alphabetical order based on the movie title
- Using AndroidX library instead of a Support Library
- Support rotation


## todo:
- Create UI tests for the application using Espresso
- When the user clicks on an item in the list, a detail fragment or dialog should open with information about the movie (i.e. title, id, rating and video flag)
- Experience more about MVVM, have API call for movie detail and dialog

## documentation:
- additional assumption user can search by id